import 'package:flutter/material.dart';
import 'package:responsive_framework/responsive_framework.dart';

void main() {
  runApp(WeatherApp());
}

enum APP_THEME { LIGHT, DARK }

class MyAppTheme {
  static ThemeData appThemeLight() {
    return ThemeData(
      brightness: Brightness.light,
      appBarTheme: AppBarTheme(
        color: Colors.blueGrey.shade900,
        iconTheme: IconThemeData(color: Colors.blue),
      ),
      iconTheme: IconThemeData(color: Colors.white),
    );
  }

  static ThemeData appThemeDark() {
    return ThemeData(
      brightness: Brightness.dark,
      appBarTheme: AppBarTheme(
        color: Colors.blueGrey.shade900,
        iconTheme: IconThemeData(color: Colors.pink),
      ),
      iconTheme: IconThemeData(color: Colors.pink),
    );
  }
}

const divider = Divider(
  color: Colors.black,
  thickness: 0.2,
);

var appbar = AppBar(
  leading: IconButton(
    icon: Icon(
      Icons.menu,
    ),
    onPressed: () {},
  ),
  title: Text("Mueang Chon Buri"),
  actions: <Widget>[],
);

var body = ListView(
  children: <Widget>[
    Row(
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              child: Text(
                "25 ํ",
                style: TextStyle(
                  fontSize: 70,
                ),
              ),
            ),
            Container(
              child: Text(
                "Mueang Chon Buri",
                style: TextStyle(
                  fontSize: 25,
                ),
              ),
            ),
            Container(
              child: Text(
                "\n 25 ํ / 21 ํ Feels like 25 ํ \n Mon, 11:18 PM \n",
                style: TextStyle(
                  fontSize: 15,
                ),
              ),
            ),
          ],
        ),
        Column(
          children: [
            Container(
              child: Icon(
                Icons.nightlight,
                color: Colors.amber.shade700,
                size: 150,
              ),
            ),
          ],
        ),
      ],
    ),
    divider,
    Container(
      margin: const EdgeInsets.only(top: 8, bottom: 8),
      child: Theme(data: ThemeData(), child: todayWeather()),
    ),
    Container(
      margin: const EdgeInsets.only(top: 8, bottom: 8),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          divider,
          weekly(),
          weekly(),
          weekly(),
          weekly(),
          weekly(),
          weekly(),
          weekly(),
          divider,
          sunriset(),
          divider,
          uvhumiditywind(),
          divider,
          maps(),
          divider,
          guide(),
          divider,
        ],
      ),
    ),
    divider,
  ],
);

Widget todayWeather() {
  return Container(
    child: SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          hours(),
          hours(),
          hours(),
          hours(),
          hours(),
          hours(),
          hours(),
          hours(),
          hours(),
          hours(),
          hours(),
          hours(),
          hours(),
          hours(),
          hours(),
          hours(),
          hours(),
          hours(),
          hours(),
          hours(),
          hours(),
          hours(),
          hours(),
          hours(),
        ],
      ),
    ),
  );
}

class WeatherApp extends StatefulWidget {
  @override
  State<WeatherApp> createState() => _WeatherAppState();
}

class _WeatherAppState extends State<WeatherApp> {
  var currentTheme = APP_THEME.LIGHT;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: MaterialApp(
        theme: currentTheme == APP_THEME.DARK
            ? MyAppTheme.appThemeLight()
            : MyAppTheme.appThemeDark(),
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          appBar: appbar,
          body: body,
          floatingActionButton: FloatingActionButton(
            child: Icon(Icons.punch_clock),
            onPressed: (() {
              setState(() {
                currentTheme == APP_THEME.DARK
                    ? currentTheme = APP_THEME.LIGHT
                    : currentTheme = APP_THEME.DARK;
              });
            }),
          ),
        ),
      ),
    );
  }
}

Widget hours() {
  return Padding(
    padding: const EdgeInsets.only(right: 12.0, left: 12.0),
    child: Column(
      children: <Widget>[
        Text("11 PM"),
        IconButton(
          icon: Icon(
            Icons.nightlight,
            color: Colors.amber.shade700,
          ),
          onPressed: () {},
        ),
        Text("25 ํ"),
      ],
    ),
  );
}

Widget weekly() {
  return Padding(
    padding: const EdgeInsets.all(15.0),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Text(
          "Today",
          style: TextStyle(fontSize: 18, fontFamily: 'Helvetica'),
        ),
        Icon(
          Icons.cloud,
          color: Colors.blue.shade300,
          size: 30,
        ),
        Icon(
          Icons.sunny,
          color: Colors.amber.shade700,
          size: 30,
        ),
        Text(
          "29 ํ",
          style: TextStyle(fontSize: 18, fontFamily: 'Helvetica'),
        ),
        Text(
          "21 ํ",
          style: TextStyle(fontSize: 18, fontFamily: 'Helvetica'),
        ),
      ],
    ),
  );
}

Widget sunriset() {
  return Padding(
    padding: const EdgeInsets.only(right: 12.0, left: 12.0),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Column(
          children: [
            Text(
              "Sunrise",
              style: TextStyle(fontSize: 25),
            ),
            Text(
              "6:39 AM",
              style: TextStyle(fontSize: 20),
            ),
            Icon(
              Icons.sunny,
              color: Colors.amber.shade700,
              size: 60,
            ),
          ],
        ),
        Column(
          children: [
            Text(
              "Sunset",
              style: TextStyle(fontSize: 25),
            ),
            Text(
              "6:01 PM",
              style: TextStyle(fontSize: 20),
            ),
            Icon(
              Icons.nightlight,
              color: Colors.amber.shade700,
              size: 60,
            ),
          ],
        ),
      ],
    ),
  );
}

Widget uvhumiditywind() {
  return Padding(
    padding: const EdgeInsets.only(right: 12.0, left: 12.0),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Column(
          children: [
            Icon(
              Icons.circle,
              color: Colors.amber.shade700,
              size: 50,
            ),
            Text(
              "UV Index",
              style: TextStyle(fontSize: 20),
            ),
            Text(
              "Low",
              style: TextStyle(fontSize: 15),
            ),
          ],
        ),
        Column(
          children: [
            Icon(
              Icons.water_drop,
              color: Colors.blue.shade300,
              size: 50,
            ),
            Text(
              "Hunidity",
              style: TextStyle(fontSize: 20),
            ),
            Text(
              "69%",
              style: TextStyle(fontSize: 15),
            ),
          ],
        ),
        Column(
          children: [
            Icon(
              Icons.wind_power,
              color: Colors.grey.shade700,
              size: 50,
            ),
            Text(
              "Wind",
              style: TextStyle(fontSize: 20),
            ),
            Text(
              "14 km/h",
              style: TextStyle(fontSize: 15),
            ),
          ],
        ),
      ],
    ),
  );
}

Widget maps() {
  return Padding(
    padding: const EdgeInsets.only(right: 12.0, left: 12.0),
    
  );
}

Widget guide() {
  return Padding(
    padding: const EdgeInsets.only(left: 12.0),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Column(
          children: [
            Text(
              "UV Index",
              style: TextStyle(fontSize: 20),
            ),
            Text(
              "UV Index",
              style: TextStyle(fontSize: 20),
            ),
            Text(
              "UV Index",
              style: TextStyle(fontSize: 20),
            ),
            Text(
              "UV Index",
              style: TextStyle(fontSize: 20),
            ),
          ],
        ),
      ],
    ),
  );
}
